import { css } from 'lit-element';

export const unstyledList = () => css`
  margin: 0;
  padding: 0;
  list-style: none;
`;

export const unstyledButton = () => css`
  text-decoration: none;
  background: transparent;
  border: none;
  margin: 0;
  padding: 0;
  outline: none;
  cursor: pointer;
`;

const appStyle = css`
  html {
    height: 100%;
  }

  body {
    width: 100%;
    height: 100%;
    overflow: auto;
    -webkit-overflow-scrolling: touch;
    margin: 0px auto;
    padding: 0px;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    color: #222;
    background-color: #fafafa;
  }
`;

export default appStyle;
