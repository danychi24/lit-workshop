import { Router } from '@vaadin/router';
import '../pages/counter/index';
import '../pages/page-not-found/index';
import '../pages/add-new-post/index';

export const ROUTES = {
  counter: '/counter',
  addNewPost: '/add-new-post',
  pageNotFound: '(.*)',
};

export const NAVIGATION = [
  { name: 'Counter', href: ROUTES.counter },
  { name: 'Add new post', href: ROUTES.addNewPost },
];

export const PAGES = [
  {
    path: '/',
    component: 'lit-counter',
  },
  {
    path: ROUTES.counter,
    component: 'lit-counter',
  },
  {
    path: ROUTES.addNewPost,
    component: 'add-new-post',
  },
  {
    path: ROUTES.pageNotFound,
    component: 'lit-page-not-found',
  },
];

export const initRouter = () => {
  const router = new Router(document.getElementById('route-content'));
  router.setRoutes(PAGES);
};
