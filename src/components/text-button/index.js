import { html, css, LitElement } from 'lit-element';
import { unstyledButton } from '../../styles';

class TextButton extends LitElement {
  static get properties() {
    return {
      onClick: { type: Function },
      disabled: { type: Boolean },
    };
  }

  static get styles() {
    return css`
      .text-button {
        ${unstyledButton()};
        padding: 15px;
        font-size: 16px;
        color: #3897f0;
      }

      .text-button[disabled] {
        color: #222;
      }
    `;
  }

  render() {
    const { onClick, disabled } = this;
    return html`
      <button class="text-button" @click="${onClick}" ?disabled="${disabled}">
        <slot />
      </button>
    `;
  }
}

customElements.define('lit-text-button', TextButton);
