import { html, css, LitElement } from 'lit-element';

export const avatarStyles = css`
  .avatar {
    border-radius: 50%;
    width: 100%;
  }
`;

class LitAvatar extends LitElement {
  static get properties() {
    return {
      photoUrl: { type: String },
    };
  }

  static get styles() {
    return [avatarStyles];
  }

  render() {
    const { photoUrl } = this;
    return html`
      <img class="avatar" src="${photoUrl}" />
    `;
  }
}

customElements.define('lit-avatar', LitAvatar);
