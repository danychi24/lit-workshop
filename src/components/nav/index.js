import { html, css } from 'lit-element';
import NavItem from './nav-item';
import { unstyledList } from '../../styles/index';
import '@material/mwc-tab';

export const navStyles = css`
  .lit-nav {
    height: 60px;
    width: 100%;
    background-color: #fff;
    border-bottom: 1px solid #dbdbdb;
  }

  .lit-nav ul {
    display: grid;
    grid-auto-flow: column;
    position: relative;
    height: 100%;
    grid-gap: 20px;
    align-items: center;
  }

  .lit-nav a {
    color: #003569;
    text-decoration: none;
  }

  .lit-nav ul {
    ${unstyledList()};
  }
`;

const LitNav = ({ navigation }) => html`
  <style>
    ${[navStyles]}
  </style>
  <nav class="lit-nav">
    <ul>
      ${navigation.map(NavItem)}
    </ul>
  </nav>
`;

export default LitNav;
