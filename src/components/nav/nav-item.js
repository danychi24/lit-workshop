import { html } from 'lit-html';

const NavItem = ({ href, name }) => html`
  <a href="${href}"><mwc-tab>${name}</mwc-tab></a>
`;

export default NavItem;
