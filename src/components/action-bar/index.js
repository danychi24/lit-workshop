import { html, css } from 'lit-element';
import '../text-button';

export const actionBarStyles = css`
  .action-bar {
    bottom: 0;
    left: 0;
    right: 0;
    position: fixed;
    height: 45px;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    background: white;
    padding: 0;
  }

  .action-bar > *:nth-child(2) {
    margin-left: auto;
  }
`;

const ActionBar = ({ onClickBack, onClickNext, isBackButtonDisabled, isNextButtonDisabled }) => html`
  <lit-container>
    <div class="action-bar">
      ${onClickBack
        ? html`
            <lit-text-button @click="${onClickBack}" ?disabled="${isBackButtonDisabled}">Back</lit-text-button>
          `
        : ''}
      ${onClickNext
        ? html`
            <lit-text-button @click="${onClickNext}" ?disabled="${isNextButtonDisabled}">Next</lit-text-button>
          `
        : ''}
    </div>
  </lit-container>
`;

export default ActionBar;
