import { css } from 'lit-element';

export const cardStyles = css`
  border-radius: 3px;
  border: 1px solid #efefef;
  background-color: #fff;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.04);
`;
