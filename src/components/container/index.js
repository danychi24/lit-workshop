import { html, css, LitElement } from 'lit-element';

export const containerStyles = css`
  .container {
    display: block;
    margin: 0 auto;
    max-width: 1600px;
  }

  .container .inner-wrap {
    padding: 0 16px;
  }

  .vertical-padding {
    padding: 24px 0;
  }
`;

class LitContainer extends LitElement {
  static get properties() {
    return {
      verticalPadding: { type: Boolean },
    };
  }

  static get styles() {
    return [containerStyles];
  }

  render() {
    const { verticalPadding = false } = this;
    return html`
      <div class="container ${verticalPadding ? 'vertical-padding' : ''}">
        <div class="inner-wrap"><slot /></div>
      </div>
    `;
  }
}

customElements.define('lit-container', LitContainer);
