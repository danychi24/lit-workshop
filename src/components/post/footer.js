import { html, css } from 'lit-element';
import Comment from './comment';
import { unstyledList } from '../../styles';

export const postFooterStyles = css`
  footer {
    padding: 16px;
  }

  .comments {
    ${unstyledList()};
    display: flex;
    flex-direction: column;
  }
`;

const PostFooter = ({ caption, username }) => html`
  <footer>
    <ul class="comments">
      ${Comment({ username, comment: caption })}
    </ul>
  </footer>
`;

export default PostFooter;
