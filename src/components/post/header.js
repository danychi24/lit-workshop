import { html, css } from 'lit-element';
import '../avatar';

export const postHeaderStyles = css`
  header {
    display: flex;
    padding: 15px 20px;
    align-items: center;
  }

  .avatar-container {
    width: 30px;
    height: 30px;
    margin-right: 10px;
    border: 1px solid #dbdbdb;
    border-radius: 50%;
  }

  .metadata-container {
    display: inline-grid;
  }

  .username {
    font-weight: bold;
    font-size: 15px;
  }
`;

const PostCardHeader = ({ avatarUrl, username, location }) => html`
  <header>
    <div class="avatar-container">
      <lit-avatar .photoUrl="${avatarUrl}"></lit-avatar>
    </div>
    <div class="metadata-container">
      <a class="username">${username}</a>
      <span>${location}</sp>
    </div>
  </header>
`;

export default PostCardHeader;
