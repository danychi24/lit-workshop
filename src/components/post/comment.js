import { html, css } from 'lit-element';

export const commentStyle = css`
  .comment {
    display: flex;
    justify-content: space-between;
    flex-shrink: 1;
    min-width: 0;
    padding: 5px 0;
    font-size: 14px;
  }

  .comment-username {
    font-weight: bold;
  }

  .comment-description {
    margin-left: 4px;
  }
`;

const Comment = ({ username, comment }) => html`
  <li>
    <a class="comment-username">${username}</a>
    <span class="comment-description">${comment}</span>
  </li>
`;

export default Comment;
