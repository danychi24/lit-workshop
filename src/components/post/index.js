import { html, css, LitElement } from 'lit-element';
import { cardStyles } from '../card';
import PostCardHeader, { postHeaderStyles } from './header';
import PostFooter, { postFooterStyles } from './footer';
import { commentStyle } from './comment';

export const postStyles = css`
  .post {
    ${cardStyles};
    max-width: 600px;
    margin: 0 auto;
    font-size: 14px;
  }

  .post-image {
    width: 100%;
  }
`;

class Post extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
    };
  }

  static get styles() {
    return [postHeaderStyles, postFooterStyles, commentStyle, postStyles];
  }

  render() {
    const { user, caption, location, photoUrl } = this.post;
    return html`
      <article>
        <div class="post">
          ${PostCardHeader({ avatarUrl: user.avatarUrl, username: user.username, location })}
          <img class="post-image" src="${photoUrl}" alt="${caption}" />
          ${PostFooter({
            username: user.username,
            caption,
          })}
        </div>
      </article>
    `;
  }
}

customElements.define('lit-post', Post);
