import { LitElement, html } from 'lit-element';

class LitCounter extends LitElement {
  static get properties() {
    return {
      count: { type: Number },
    };
  }

  constructor() {
    super();
    this.count = 0;
  }

  updated(changedProperties) {
    if (changedProperties.has('count') && this.count === 10) {
      this.count = 0;
    }
  }

  _updateCount = (count) => {
    this.count = count;
  };

  render() {
    const { count } = this;
    return html`
      <lit-container>
        <p>
          Count until 10: ${count}
        </p>
        <p>
          <button @click="${() => this._updateCount(count + 1)}">Increment</button>
          <button @click="${() => this._updateCount(count - 1)}">Decrement</button>
        </p>
      </lit-container>
    `;
  }
}

window.customElements.define('lit-counter', LitCounter);
