import { html, css } from 'lit-element';

export const selectedImageStyles = css`
  .selected-image {
    object-fit: contain;
    background: #efefef;
    width: 100%;
    height: 20vh;
  }

  @media (min-width: 768px) {
    .selected-image {
      height: 40vh;
    }
  }
`;

const handleBrokenImage = (e) => {
  e.target.src = 'https://aveb.nl/wp-content/uploads/2016/11/orionthemes-placeholder-image-1.png';
};

const SelectedImage = ({ photoUrl }) =>
  html`
    <img class="selected-image" .src=${photoUrl} @error="${handleBrokenImage}" />
  `;

export default SelectedImage;
