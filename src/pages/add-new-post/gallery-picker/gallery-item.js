import { html, css } from 'lit-element';
import { unstyledButton } from '../../../styles';

export const galleryItemStyles = css`
  .gallery-item {
    height: 100%;
  }

  .gallery-item button {
    ${unstyledButton()};
    height: 100%;
    color: #003569;
    cursor: pointer;
  }

  .gallery-item img {
    width: 100%;
    height: 100%;
  }

  .gallery-item .selected {
    opacity: 0.4;
  }
`;

const GalleryItem = ({ photoUrl, isSelected, onClick }) => html`
  <li class="gallery-item">
    <button @click="${onClick}" class="${isSelected ? 'selected' : ''}">
      <img src="${photoUrl}" />
    </button>
  </li>
`;

export default GalleryItem;
