import { html, LitElement } from 'lit-element';
import SelectedImage, { selectedImageStyles } from './selected-image';
import Gallery, { galleryStyles } from './gallery';
import { galleryItemStyles } from './gallery-item';

class GalleryPicker extends LitElement {
  static get properties() {
    return {
      images: { type: Array },
      selectedImage: { type: Object },
      onClickImage: { type: Function },
    };
  }

  static get styles() {
    return [selectedImageStyles, galleryStyles, galleryItemStyles];
  }

  render() {
    const { images, selectedImage = {}, onClickImage } = this;
    return html`
      <div class="gallery-picker">
        ${SelectedImage({ photoUrl: selectedImage.photoUrl })} ${Gallery({ images, selectedImage, onClickImage })}
      </div>
    `;
  }
}

customElements.define('gallery-picker', GalleryPicker);
