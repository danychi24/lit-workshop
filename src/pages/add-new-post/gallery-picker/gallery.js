import { html, css } from 'lit-element';
import GalleryItem from './gallery-item';
import { unstyledList } from '../../../styles';

export const galleryStyles = css`
  .grid {
    ${unstyledList()};
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(220px, 1fr));
    max-height: 45vh;
    overflow-y: scroll;
  }
`;

const Gallery = ({ images, selectedImage = {}, onClickImage }) => html`
  <ul class="grid">
    ${
      Array.isArray(images)
        ? images.map((image) =>
            GalleryItem({
              photoUrl: image.photoUrl,
              isSelected: selectedImage.id === image.id,
              onClick: () => onClickImage(image),
            })
          )
        : ''
    }
  </div>
`;

export default Gallery;
