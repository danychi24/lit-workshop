import { css, html } from 'lit-element';
import '../../../components/avatar';

export const detailsFormStyle = css`
  .caption-wrap {
    display: grid;
    grid-template-columns: 1fr 5fr 2fr;
    grid-gap: 0 15px;
    background-color: #fff;
    border: 1px solid #efefef;
    padding: 16px;
    margin-bottom: 10px;
  }

  .location-wrap {
    background-color: #fff;
    border: 1px solid #efefef;
    padding: 16px;
  }

  .caption-input {
    background: 0 0;
    border: 0;
    color: #262626;
    outline: 0;
    overflow: auto;
    overflow-wrap: break-word;
    resize: none;
    font-size: 14px;
    line-height: 18px;
  }

  label {
    align-items: center;
    display: grid;
    grid-template-columns: 1fr 5fr 2fr;
    white-space: nowrap;
  }

  .location-input {
    background: 0 0;
    border: 0;
    color: #262626;
    outline: 0;
    overflow: auto;
    overflow-wrap: break-word;
    font-size: 14px;
    line-height: 18px;
    margin-left: 15px;
    flex: 1;
  }

  .caption-wrap img {
    width: 100%;
  }
`;

const DetailsForm = ({ selectedImage, profileImage, onSavePost }) => html`
  <form id="addAPostForm" @submit="${onSavePost}">
    <section class="caption-wrap">
      <lit-avatar .photoUrl="${profileImage}"></lit-avatar>
      <textarea 
        class="caption-input" 
        placeholder="Write a caption…"
        autoComplete="off"
        autoCorrect="off"
        name="caption"
        ></textarea>
      <img .src="${selectedImage.photoUrl}" />
    </section>
    <section class="location-wrap">
      <label>
          <strong>Add location</strong>
          <input class="location-input" name="location"></input>
      </label>
  </section>
</form>
`;

export default DetailsForm;
