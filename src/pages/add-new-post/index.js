import { LitElement, html } from 'lit-element';
import './gallery-picker/index';
import { fetchGallery } from '../../services/gallery';
import ActionBar, { actionBarStyles } from '../../components/action-bar/index';
import DetailsForm, { detailsFormStyle } from './details-form/index';
import { createPost } from '../../services/posts';
import { fetchProfile } from '../../services/profile';
import { buildPost } from '../../utils/posts';
import '../../components/post/index';

class AddNewPost extends LitElement {
  static get properties() {
    return {
      step: { type: Number },
      images: { type: Array },
      selectedImage: { type: Function },
      post: { type: Object },
      user: { type: Object },
    };
  }

  static get styles() {
    return [actionBarStyles, detailsFormStyle];
  }

  get form() {
    return this.shadowRoot.querySelector('#addAPostForm');
  }

  _serializeForm() {
    return Object.fromEntries(new FormData(this.form).entries());
  }

  async connectedCallback() {
    super.connectedCallback();
    this._fetchPageData();
    this.user = await fetchProfile();
    this.step = 0;
  }

  _handleGoBack = () => {
    this.step = this.step - 1;
  };

  _handleGoNext = () => {
    this.step = this.step + 1;
  };

  _fetchPageData = async () => {
    this.images = await fetchGallery();
  };

  _handleSelectImage = (image) => {
    this.selectedImage = image;
  };

  _handleSubmitForm = async (e) => {
    e.preventDefault();
    const { caption, location } = this._serializeForm();
    const post = buildPost(this.selectedImage.photoUrl, caption, location, this.user);
    this.post = await createPost(post);
    this._handleGoNext();
  };

  render() {
    const {
      step,
      images,
      selectedImage,
      _handleSelectImage,
      _handleGoBack,
      _handleGoNext,
      _handleSubmitForm,
      post,
      user,
    } = this;
    return html`
      <lit-container>
        ${step === 0
          ? html`
              <gallery-picker
                .images="${images}"
                .selectedImage="${selectedImage}"
                .onClickImage="${_handleSelectImage}"
              ></gallery-picker>

              ${ActionBar({
                onClickBack: _handleGoBack,
                onClickNext: _handleGoNext,
                isBackButtonDisabled: true,
              })}
            `
          : ''}
        ${step === 1
          ? html`
              ${DetailsForm({
                selectedImage,
                profileImage: user.avatarUrl,
                onSavePost: _handleSubmitForm,
              })}
              ${ActionBar({
                onClickBack: _handleGoBack,
                onClickNext: _handleSubmitForm,
              })}
            `
          : ''}
      </lit-container>
      <lit-container verticalPadding>
        ${step === 2
          ? html`
              <lit-post .post=${post}></lit-post>
            `
          : ''}
      </lit-container>
    `;
  }
}

customElements.define('add-new-post', AddNewPost);
