import { LitElement, html } from 'lit-element';

class LitPageNotFound extends LitElement {
  render() {
    return html`
      <h1>Page not found</h1>
    `;
  }
}

window.customElements.define('lit-page-not-found', LitPageNotFound);
