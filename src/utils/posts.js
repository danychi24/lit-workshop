export const buildPost = (photoUrl, caption, location, user) => ({
  id: new Date().getUTCMilliseconds(),
  photoUrl,
  caption,
  createdAt: new Date().toISOString(),
  location,
  likesCount: 0,
  likedByUser: false,
  comments: [],
  user: {
    id: user.id,
    username: user.username,
    avatarUrl: user.avatarUrl,
  },
});
