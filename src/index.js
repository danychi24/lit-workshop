import { LitElement, html } from 'lit-element';

import './components/container/index';
import style from './styles/index';
import { initRouter, NAVIGATION } from './router/index';
import LitNav from './components/nav/index';

export class LitWorkshop extends LitElement {
  firstUpdated() {
    initRouter();
  }

  render() {
    return html`
      <style>
        ${[style]}
      </style>
      <lit-container>
        <header>${LitNav({ navigation: NAVIGATION })}</header>
      </lit-container>

      <main id="route-content"></main>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

window.customElements.define('lit-workshop', LitWorkshop);
